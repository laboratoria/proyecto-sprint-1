var estudiantes = [];

function obtenerListaEstudiantes() {
    return estudiantes;
}

function agregarEstudiante() {
    var nombre = prompt("Nombre de estudiante:");
    var puntosTecnicos = prompt("Puntos técnicos:");
    var puntosHSE = prompt("Puntos HSE:");
    var estudiante = {
        nombre: nombre,
        tecnico: puntosTecnicos,
        hse: puntosHSE
    };
    estudiantes.push(estudiante);
    return estudiante;
}

function mostrar(estudiante) {
    var resultado = "";
    /*
    <div class="row">
        <div class="col s12 m6">
          <div class="card blue-grey darken-1">
            <div class="card-content white-text">
            */
    resultado += "<div class='row'>";
    resultado += "<div class='col m12'>";
    resultado += "<div class='card blue-grey darken-1'>";
    resultado += "<div class='card-content white-text'>";
    resultado += "<p><strong>Nombre:</strong> " + estudiante.nombre + "</p>";
    resultado += "<p><strong>Puntos Técnicos:</strong> " + estudiante.tecnico + "</p>";
    resultado += "<p><strong>Puntos HSE:</strong> " + estudiante.hse + "</p>";
    resultado += "</div>";
    resultado += "</div>";
    resultado += "</div>";
    resultado += "</div>";
    return resultado;
}

function mostrarLista(estudiantes) {
    var resultado = "";
    estudiantes.forEach(function (estudiante) {
        resultado += mostrar(estudiante);
    });
    return resultado;
}

function buscar(nombre, estudiantes) {
    var estudiantesBuscados = estudiantes.filter(function (estudiante) {
        return estudiante.nombre.toLowerCase() === nombre.toLocaleLowerCase();
    });
    return estudiantesBuscados;
}

function topTecnico(estudiantes) {
    var estudiantesTop = estudiantes.sort(function (a, b) {
        return b.tecnico - a.tecnico;
    });
    return estudiantesTop;
}

function topHSE(estudiantes) {
    var estudiantesTop = estudiantes.sort(function (a, b) {
        return b.hse - a.hse;
    });
    return estudiantesTop;
}